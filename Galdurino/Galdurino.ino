#include <EEPROM.h>


#define VPP A0
#define VCC A1
#define REF A2
#define STB A3
//13 is the spi - clock
#define SCK 13
#define SDO 12
#define SDI 11
#define CSR 10
#define RA0 9
#define PV 8
#define RA5 7
#define RA4 6
#define RA3 5
#define RA2 4
#define RA1 3

float voltage =0.0;
boolean VPPOn = false;
boolean VCCOn = false;
int poti= 0;
byte delays[4] = {0,0,0,0};
int RA[] ={9,3,4,5,6,7};
int RA22[] ={4,3,9,5,6,7}; //bug: I made a mistake on connecting RA0..2: Reverse!
String inputString ="";
boolean stringComplete = false;
boolean galline[160];
byte galType=0;
byte ventor=0;
String version="1.0";

void setup() {
  
  inputString.reserve(200);

  //configure pins
  pinMode(VPP, OUTPUT);
  digitalWrite(VPP, LOW);
  pinMode(VCC, OUTPUT);
  digitalWrite(VCC, LOW);

  pinMode(RA0, OUTPUT);
  pinMode(RA1, OUTPUT);
  pinMode(RA2, OUTPUT);
  pinMode(RA3, OUTPUT);
  pinMode(RA4, OUTPUT);
  pinMode(RA5, OUTPUT);
  pinMode(PV, OUTPUT);
  pinMode(STB, OUTPUT);
  pinMode(SCK, OUTPUT);
  pinMode(SDI, OUTPUT);
  pinMode(CSR, OUTPUT);
  pinMode(SDO, INPUT);

  digitalWrite(RA0, LOW);
  digitalWrite(RA1, LOW);
  digitalWrite(RA2, LOW);
  digitalWrite(RA3, LOW);
  digitalWrite(RA4, LOW);
  digitalWrite(RA5, LOW);
  digitalWrite(PV, LOW);
  digitalWrite(SDI, LOW);
  digitalWrite(STB, HIGH);
  digitalWrite(CSR, LOW);
  digitalWrite(SCK, LOW);
  voltage = ReadVoltage();
  VPPOn = false;
  VCCOn = false;
  digitalPotWrite(0,poti);
  //read default values:
  delays[0] = EEPROM.read(0);
  delays[1] = EEPROM.read(1);
  delays[2] = EEPROM.read(2);
  delays[3] = EEPROM.read(3);
  
  // initialize Serial:
  Serial.begin(9600);
}

void loop() {

  if (stringComplete) {
    inputString.toUpperCase();
    inputString = inputString.substring(0,inputString.length()-1);
    if(inputString.equals("POWER OFF")){
      Serial.println("Setting VCC/VPP off");
      setVCC(false);
      setVPP(false);
    }

    if(inputString.equals("VCC ON")){
      Serial.println("Setting VCC on");
      setVCC(true);
    }
    if(inputString.equals("VCC OFF")){
      Serial.println("Setting VCC off");
      setVCC(false);
    }
    if(inputString.equals("VPP ON")){
      Serial.println("Setting VPP on");
      setVPP(true);
    }
    if(inputString.equals("VPP OFF")){
      Serial.println("Setting VPP off");
      setVPP(false);
    }
    if(inputString.equals("STATUS")){
      printStatus();
    }
    
    if(inputString.equals("HELP") ||inputString.equals("?")){
      printHelp();
    }

    if(inputString.equals("VERSION")){
      Serial.println("Galdurnio Version "+version);
    }

    if(inputString.startsWith("IDENTIFY")){
      //format is: IDENTIFY <VPP>
      int end=inputString.indexOf(" ");
      //cut of "IDENTIFY"
      inputString=inputString.substring(end+1);
      poti  = inputString.substring(0).toInt();
      //read and print galtype and ventor
      readType(true);
      Serial.println("ID end");
    }

    if(inputString.startsWith("SETDELAYS")){            
      int params[]={0,0,0,0};
      //format is: SETDELAYS  <delay voltage adjust> <delayVpp2Vcc> <delayVPP2read> <delayVcc2VPPCyc>
      int end=inputString.indexOf(" ");
      //cut of "SETDELAYS"
      inputString=inputString.substring(end+1);
      end=inputString.indexOf(" ");
      int vals =0;      
      while(inputString.length()>0 and end>0){
        if(vals<4)
          params[vals] = inputString.substring(0,end).toInt();
        vals++;
        inputString=inputString.substring(end+1);
        end=inputString.indexOf(" ");
        if(end<0 && inputString.length()>0)//last token!
          end=inputString.length();
      }
      if(vals ==4){ //correct number of parameters?
        int i;
        for (i=0; i<4; ++i){
          delays[i] = params[i];
          EEPROM.write(i, delays[i]);
        }
      }
      else{
        Serial.println("ERROR!");
        printHelp();
      }
    }


    if(inputString.startsWith("READ")){      
      
      int adr = 0;
      //boolean bits[140];
      int data=1;
      int length =64;
      int volt=0;
      int params[]={0,64,1};
      //format is: READ <address> <length> <Data=1/Fuse=0>
      int end=inputString.indexOf(" ");
      //cut of "READ"
      inputString=inputString.substring(end+1);
      end=inputString.indexOf(" ");
      if(end<0){ 
        end = inputString.length()-1;
      }
      int vals =0;      
      while(inputString.length()>0 and end>0){
        if(vals<3)
          params[vals] = inputString.substring(0,end).toInt();
        if(vals==3){
          poti = inputString.substring(0,end).toInt();
        }
        vals++;
        inputString=inputString.substring(end+1);
        end=inputString.indexOf(" ");
        if(end<0 && inputString.length()>0)//last token!
          end=inputString.length();
      }
      if(vals == 3  || vals == 4){ //correct number of parameters?
        readOneColumn(params[0],params[1]);
        printGalLine(params[1]);
      }
      else{
        Serial.println("ERROR!");
        printHelp();
      }
    }

    if(inputString.startsWith("16V8READ")){      
      
      int adr = 0;

      //format is: 16V8READ <VPP>
      int end=inputString.indexOf(" ");
      //cut of "READ"
      inputString=inputString.substring(end+1);
      end=inputString.indexOf(" ");
      if(end<0 && inputString.length()>0)//last token!
          end=inputString.length();
      int vals =0;      
      while(inputString.length()>0 and end>0){
        if(vals==0){
          poti = inputString.substring(0,end).toInt();
        }
        vals++;
        inputString=inputString.substring(end+1);
        end=inputString.indexOf(" ");
        if(end<0 && inputString.length()>0)//last token!
          end=inputString.length();
      }
      if(vals == 1 ){ //correct number of parameters?
        //read and print galtype and ventor
        readType(true);
        Serial.println("PES:");
        readPES(true);
        Serial.println("FUSEMAP:");
        for(adr=0; adr<32;adr++){
          readOneColumn(adr,64);
          printGalLine(64);
        }    
        //ues 
        Serial.println("UES:");
        readOneColumn(32,64);
        printGalLine(64);
        //cfg
        Serial.println("CFG:");
        readOneColumn(60,82);
        printGalLine(82);
        Serial.println("END");
      }
      else{
        Serial.println("ERROR!");
        printHelp();
      }
    }

    if(inputString.startsWith("20V8READ")){      
      
      int adr = 0;

      //format is: 20V8READ <VPP>
      int end=inputString.indexOf(" ");
      //cut of "READ"
      inputString=inputString.substring(end+1);
      end=inputString.indexOf(" ");
      if(end<0 && inputString.length()>0)//last token!
        end=inputString.length();
      int vals =0;      
      while(inputString.length()>0 and end>0){
        if(vals==0){
          poti = inputString.substring(0,end).toInt();
        }
        vals++;
        inputString=inputString.substring(end+1);
        end=inputString.indexOf(" ");
        if(end<0 && inputString.length()>0)//last token!
          end=inputString.length();
      }
      if(vals == 1 ){ //correct number of parameters?
        //read and print galtype and ventor
        readType(true);
        Serial.println("PES:");
        readPES(true);
        Serial.println("FUSEMAP:");
        for(adr=0; adr<40;adr++){
          readOneColumn(adr,64);
          printGalLine(64);
        }    
        //ues 
        Serial.println("UES:");
        readOneColumn(40,64);
        printGalLine(64);
        //cfg
        Serial.println("CFG:");
        readOneColumn(60,82);
        printGalLine(82);
        Serial.println("END");
      }
      else{
        Serial.println("ERROR!");
        printHelp();
      }
    }
    
    if(inputString.startsWith("22V10READ")){  
          
      
      int end=inputString.indexOf(" ");
      //cut of "READ"
      inputString=inputString.substring(end+1);
      end=inputString.indexOf(" ");
      if(end<0 && inputString.length()>0)//last token!
          end=inputString.length();
      int vals =0;      
      while(inputString.length()>0 and end>0){
        if(vals==0){
          poti = inputString.substring(0,end).toInt();
        }
        vals++;
        inputString=inputString.substring(end+1);
        end=inputString.indexOf(" ");
        if(end<0 && inputString.length()>0)//last token!
          end=inputString.length();
      }
      if(vals == 1 ){ //correct number of parameters?
        readPES22V10(false);
        Serial.println("GAL:");
        Serial.println(galType, HEX);
        Serial.println("VENTOR:");
        Serial.println(ventor, HEX);
        Serial.println("PES:");
        printGalLine(138);
        Serial.println("FUSEMAP:");
        readFuseMap22V10();
        Serial.println("UES:");
        readOneLine(0,44,true);
        printGalLine(138);
        Serial.println("CFG:");
        readCFGLine(16,0,true);
        printGalLine(20);
        Serial.println("END");
      }
      else{
        Serial.println("ERROR!");
        printHelp();
      }
    }
    if(inputString.startsWith("SETPOTI")){      
      poti  = inputString.substring(8).toInt();
      digitalPotWrite(0,poti);
      delay(300);
        
      voltage = ReadVoltage();  
      String status = "VPP-voltage: "+String(voltage,1)+"V";
      Serial.println(status);
    }

    inputString = "";
    stringComplete = false;
  }  
}

void readOneColumn(int adr, int num){
  
  //init
  digitalWrite(PV, LOW);
  digitalWrite(SDI, LOW);
  digitalWrite(STB, HIGH);
  digitalWrite(SCK, LOW);
  digitalPotWrite(0,poti);
  delay(delays[0]);
  setVPP(true);
  delay(delays[1]);
  setVCC(true);
  delay(delays[2]);
  readAddress(adr,num);
  setVPP(false);
  setVCC(false);
}

void readOneLine(int adr, int line, boolean fuse){
  digitalWrite(PV, LOW);
  digitalWrite(SDI, LOW);
  digitalWrite(STB, HIGH);
  digitalWrite(SCK, LOW);
  digitalPotWrite(0,poti);
  delay(delays[0]);
  setVPP(true);
  delay(delays[1]);
  setVCC(true);
  if(delays[3]>=0){
     delay(delays[2]);
     setVPP(false);
     delay(5);
     setVPP(true);
     delay(delays[3]);
   }
   readAddressLine(adr,line,138,fuse);
   setVPP(false);
   setVCC(false);
}


void readFuseMap22V10(){
  int line;
  digitalWrite(PV, LOW);
  digitalWrite(SDI, LOW);
  digitalWrite(STB, HIGH);
  digitalWrite(SCK, LOW);
  digitalPotWrite(0,poti);
  delay(delays[0]);
  setVPP(true);
  delay(delays[1]);
  setVCC(true);
  if(delays[3]>=0){
    delay(delays[2]);
    setVPP(false);
    delay(5);
    setVPP(true);
    delay(delays[3]);
  }
  readAddressLine(0, 0, 138, true);
  printGalLine(138);
  for(line=1; line<44;line++){
    readAddressLine(0, line, 138, true);
    printGalLine(138);
  }    
  setVPP(false);
  setVCC(false);
}

void readCFGLine(int adr, int line, boolean fuse){
  digitalWrite(PV, LOW);
  digitalWrite(SDI, LOW);
  digitalWrite(STB, HIGH);
  digitalWrite(SCK, LOW);
  digitalPotWrite(0,poti);
  delay(delays[0]);
  setVPP(true);
  delay(delays[1]);
  setVCC(true);
  if(delays[3]>=0){
     delay(delays[2]);
     setVPP(false);
     delay(5);
     setVPP(true);
     delay(delays[3]);
   }
   readAddressLine(adr,line,20,fuse);
   setVPP(false);
   setVCC(false);
}


float ReadVoltage(){
  int values=0, i;
  for(i=0;i<3; ++i) 
    values += analogRead(REF);
  return (float)values * (12.5*5.0 / (3*1023.0));
}

void printHelp(){
  Serial.println("Available commands (case insensitive):");
  Serial.println("HELP : Prints this text");
  Serial.println("POWER OFF : Switches VCC/VPP off");
  Serial.println("VCC ON/OFF : Switches VCC on/off");
  Serial.println("VPP ON/OFF : Switches VCC on/off");
  Serial.println("SETPOTI <value> : Sets the VPP-power poti to a specified value (0-255)");
  Serial.println("SETDELAYS <delay voltage adjust> <delayVpp2Vcc> <delayVPP2read> <delayVcc2VPPCyc> : sets the delays");
  Serial.println("STATUS : Prints the status");  
  Serial.println("READ <address> <length> <Data=1/Fuse=0> <optional:POTIVAL>: Reads one row from a 16V8 / 20V8 device");
  Serial.println("IDENTIFY <POTIVAL> : Reads the device and ventor information");
  Serial.println("16V8READ <POTIVAL> : Reads a 16V8 device");
  Serial.println("20V8READ <POTIVAL> : Reads a 20V8 device");
  Serial.println("22V10READ <POTIVAL> : Reads a 22V10 device");
}

void printStatus(){
  String status = "Delays: " + String(delays[0],DEC)+" "+String(delays[1],DEC)+" "+String(delays[2],DEC)+" "+String(delays[3],DEC);
  Serial.println(status);
  
  status = "Poti: " +String(poti,DEC);
  Serial.println(status);
  
  if(VCCOn==1)
    Serial.println("VCC on");
  else  
    Serial.println("VCC off");
  
  if(VPPOn==1)
    Serial.println("VPP on");
  else  
    Serial.println("VPP off");
  
  voltage = ReadVoltage();  
  status = "VPP-voltage: "+String(voltage,1);
  Serial.println(status);
}

void setVCC(boolean on){
  VCCOn = on;
  digitalWrite(VCC,on);
}


void setVPP(boolean on){
  VPPOn = on;
  digitalWrite(VPP,on);
}


void readAddress(int adr, int length){
  int i=0;
  boolean bits;

  //init
  
  digitalWrite(PV, LOW); //read!
  for(i=0; i<6; ++i){
    if(adr&1)
      digitalWrite(RA[i],HIGH);
    else
      digitalWrite(RA[i],LOW);
    adr =adr>>1;
  }
  delayMicroseconds(5);  

  
  digitalWrite(STB, LOW);
  delayMicroseconds(5);
  digitalWrite(STB, HIGH);

  delayMicroseconds(5);
  for(i=0;i<length;++i){
    galline[i]=digitalRead(SDO);
    digitalWrite(SCK, HIGH);
    delayMicroseconds(5);
    digitalWrite(SCK, LOW);
    delayMicroseconds(5);    
  }
}

void readPES(boolean printIt){
  readOneColumn(58,64);
  //galType
  int i;
  for(i=23; i>15; --i){
    bitWrite(galType,i-16, galline[i]);
  }
  //ventor
  for(i=31; i>23; --i){
    bitWrite(ventor,i-24, galline[i]);
  }
  if(printIt){
    printGalLine(64);
  }
}

void readPES22V10(boolean printIt){
  readOneLine(0,58,true);
  //galType
  int i;
  for(i=23; i>15; --i){
    bitWrite(galType,i-16, galline[i]);
  }
  //ventor
  for(i=31; i>23; --i){
    bitWrite(ventor,i-24, galline[i]);
  }
  if(printIt){
    printGalLine(138);
  }
}

void readType(boolean printIt){
  readPES(false);
  if(printIt){
    Serial.println("GAL:");
    Serial.println(galType, HEX);
    Serial.println("VENTOR:");
    Serial.println(ventor, HEX);
  }
}

void printGalLine(int length){
  for(int i=0;i<length;++i){
    Serial.print(galline[i]);    
  }
  Serial.println();
}

void readAddressLine(int adr, int line, int length, boolean nFuse){
  int i;
  boolean bits;

  for(i=0; i<6; ++i){
    if(adr&1)
      digitalWrite(RA22[i],HIGH);
    else
      digitalWrite(RA22[i],LOW);
    adr =adr>>1;
  }

  if(!nFuse){
    digitalWrite(SDI, nFuse);
    delayMicroseconds(5);
    digitalWrite(SCK, HIGH);
    delayMicroseconds(5);
    digitalWrite(SCK, LOW);
    
  }
  else{
    digitalWrite(SDI,LOW);
    /*
    for(i=0; i<132; ++i){
      delayMicroseconds(5);
      digitalWrite(SCK, HIGH);
      delayMicroseconds(5);
      digitalWrite(SCK, LOW);      
    }
    */
    delayMicroseconds(5);
    for(i=0; i<6; ++i){
      if(line&1)
        digitalWrite(SDI,HIGH);
      else
        digitalWrite(SDI,LOW);
      line =line>>1;
      delayMicroseconds(5);
      digitalWrite(SCK, HIGH);
      delayMicroseconds(5);
      digitalWrite(SCK, LOW);
    }
  }
  
  delayMicroseconds(5);
  digitalWrite(STB, LOW);
  delayMicroseconds(10);
  digitalWrite(STB, HIGH);
  delayMicroseconds(5);

  galline[0]=digitalRead(SDO);
  for(i=1;i<length;++i){
    digitalWrite(SCK, HIGH);
    delayMicroseconds(5);
    digitalWrite(SCK, LOW);
    delayMicroseconds(5);
    galline[i]=digitalRead(SDO);
  }
}


void writeOneBitSPI(boolean bit){
  digitalWrite(SDI, bit);
  delayMicroseconds(5);
  digitalWrite(SCK, HIGH);
  delayMicroseconds(5);
  digitalWrite(SCK, LOW);
  
}

void digitalPotWrite(int address, int value) {
  int mask;
  // take the SS pin low to select the chip:
  digitalWrite(CSR,LOW);

  //mask=2;
  //  send in the address via SPI:
  //writeOneBitSPI(address&mask);
  //mask=mask>>1;  
  //writeOneBitSPI(address&mask);
  //  send the value via SPI:
  //mask=0x80;
  //for(int i=0; i<8; ++i,mask=mask>>1){
  //  writeOneBitSPI(value&mask);
  //}

  //MCP4151: write 6 zeros for writing wiper one
  // 4 Address bits: shoult be 0000
  // 2 command Bits: 00
  mask=0x08;
  for(int i=0; i<4;++i){
    writeOneBitSPI(address&mask);
  }

  for(int i=0; i<2;++i){
    writeOneBitSPI(0);
  }

  //  send the value via SPI:
  mask=0x200;
  for(int i=0; i<10; ++i,mask=mask>>1){
    writeOneBitSPI(value&mask);
  }

  
  digitalWrite(CSR,HIGH);
}

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}
