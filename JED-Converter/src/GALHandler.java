
public class GALHandler {

	String name ,ventor ;
	int id, ventorID;
	int uesOffset;
	boolean[][] fuses;
	boolean[] config;
	boolean[] UES;
	int[] cfgMap;

	static int[] cfg16v8 	= {2128,2129,2130,2131,2132,2133,2134,2135,2136,2137,2138,2139,2140,2141,2142,2143,2144,2145,2146,2147,2148,2149,2150,2151,2152,2153,2154,2155,2156,2157,2158,2159,2048,2049,2050,2051,2193,2120,2121,2122,2123,2124,2125,2126,2127,2192,2052,2053,2054,2055,2160,2161,2162,2163,2164,2165,2166,2167,2168,2169,2170,2171,2172,2173,2174,2175,2176,2177,2178,2179,2180,2181,2182,2183,2184,2185,2186,2187,2188,2189,2190,2191};
	static int[] cfg16v8bcd = {2048,2049,2050,2051,2193,2120,2121,2122,2123,2128,2129,2130,2131,2132,2133,2134,2135,2136,2137,2138,2139,2140,2141,2142,2143,2144,2145,2146,2147,2148,2149,2150,2151,2152,2153,2154,2155,2156,2157,2158,2159,2160,2161,2162,2163,2164,2165,2166,2167,2168,2169,2170,2171,2172,2173,2174,2175,2176,2177,2178,2179,2180,2181,2182,2183,2184,2185,2186,2187,2188,2189,2190,2191,2124,2125,2126,2127,2192,2052,2053,2054,2055};
	static int[] cfg20v8 	= {2640,2641,2642,2643,2644,2645,2646,2647,2648,2649,2650,2651,2652,2653,2654,2655,2656,2657,2658,2659,2660,2661,2662,2663,2664,2665,2666,2667,2668,2669,2670,2671,2560,2561,2562,2563,2705,2632,2633,2634,2635,2636,2637,2638,2639,2704,2564,2565,2566,2567,2672,2673,2674,2675,2676,2677,2678,2679,2680,2681,2682,2683,2684,2685,2686,2687,2688,2689,2690,2691,2692,2693,2694,2695,2696,2697,2698,2699,2700,2701,2702,2703};
	static int[] cfg20v8bcz = {2560,2561,2562,2563,2705,2632,2633,2634,2635,2640,2641,2642,2643,2644,2645,2646,2647,2648,2649,2650,2651,2652,2653,2654,2655,2656,2657,2658,2659,2660,2661,2662,2663,2664,2665,2666,2667,2668,2669,2670,2671,2672,2673,2674,2675,2676,2677,2678,2679,2680,2681,2682,2683,2684,2685,2686,2687,2688,2689,2690,2691,2692,2693,2694,2695,2696,2697,2698,2699,2700,2701,2702,2703,2636,2637,2638,2639,2704,2564,2565,2566,2567};
	static int[] cfg22v10   = {5809,5808,5811,5810,5813,5812,5815,5814,5817,5816,5819,5818,5821,5820,5823,5822,5825,5824,5827,5826};

	private void fixNames() {
		this.name = getName(this.id);
		switch(this.id) {
		case 0x0:
			//GAL16V8
			this.UES = new boolean[64];
			this.fuses = new boolean[32][64];
			this.config = new boolean[82];
			this.uesOffset= 2056;
			this.cfgMap = cfg16v8;
			break;
		case 0x1A:
			//GAL16V8B/C/D
			this.UES = new boolean[64];
			this.fuses = new boolean[32][64];
			this.config = new boolean[82];
			this.uesOffset= 2056;
			this.cfgMap = cfg16v8bcd;
			break;
		case 0x20:
			//GAL20V8
			this.UES = new boolean[64];
			this.fuses = new boolean[40][64];
			this.config = new boolean[82];
			this.uesOffset= 2568;
			this.cfgMap = cfg20v8;
			break;
		case 0x3A:
			//GAL20V8B/C/D
			this.UES = new boolean[64];
			this.fuses = new boolean[40][64];
			this.config = new boolean[82];
			this.uesOffset= 2568;
			this.cfgMap = cfg20v8bcz;
			break;
		case 0x48:
			//GAL22V10
			this.UES = new boolean[64];
			this.fuses = new boolean[44][132];
			this.config = new boolean[20];
			this.uesOffset= 5828;
			this.cfgMap = cfg22v10;
			break;
		case 0x49:
			//GAL22V10B/C/D
			this.UES = new boolean[64];
			this.fuses = new boolean[44][132];
			this.config = new boolean[20];
			this.uesOffset= 5828;
			this.cfgMap = cfg22v10;
			break;
		default:
			//UNKNOWN
			//avoid null pointer exceptions
			this.UES = new boolean[0];
			this.fuses = new boolean[0][0];
			this.config = new boolean[0];
			this.uesOffset= 0;
			this.cfgMap = new int[0];
			break;
		}
	}
	
	
	public static String getName(int id) {
		switch(id) {
		case 0x0:
			return "GAL16V8";
		case 0x1A:
			return "GAL16V8B/C/D";
		case 0x20:
			return "GAL20V8";
		case 0x3A:
			return "GAL20V8B/C/D";
		case 0x48:
			return "GAL22V10";
		case 0x49:
			return "GAL22V10B/C/D";
		default:
			return "UNKNOWN";
		}
	}
	public static String getVentor(int ventorID) {

		
		switch(ventorID) {
		case 0xA1:
			return "Lattice";
		case 0x8F:
			return "National Semiconductor";
		case 0x20:
			return "STMicrosystems";
		default:
			return "UNKNOWN";
		}
	}
	
	private void readUES(String UESin) {
		for(int i=0; i< this.UES.length && i< UESin.length(); i++) {
			if(UESin.charAt(i)=='1') {
				this.UES[i] =true;
			}
			else {
				this.UES[i] =false;
			}
		}
	}
	
	private void readCFG(String CFGin) {
		for(int i=0; i< this.config.length && i< CFGin.length(); i++) {
			if(CFGin.charAt(i)=='1') {
				this.config[i] =true;
			}
			else {
				this.config[i] =false;
			}
		}
	}
	
	private void readFuseLine(String fuseline, int line) {
		for(int i=0; i< this.fuses[line].length && i< fuseline.length(); i++) {
			if(fuseline.charAt(i)=='1') {
				this.fuses[line][i] =true;
			}
			else {
				this.fuses[line][i] =false;
			}
		}
	}
	
	
	public GALHandler(String serialDump) {
		String[] tokens = serialDump.split(System.getProperty("line.separator"));
		boolean readingFuses =false;
		int line =0;
		for(int i=0; i< tokens.length; ) { //do a variable jumpwidth
			if(readingFuses) {
				if(tokens[i].contains(":")) {
					//stop reading fuses
					readingFuses =false;
				}
				else {
					readFuseLine(tokens[i],line);
					i++;
					line++;
				}
			}
			else {
				switch(tokens[i]) {
				case "GAL:":
					this.id=Integer.decode("0x"+tokens[i+1]);
					i+=2;
					this.fixNames();
					if(this.name.equals("UNKNOWN")) { //aboard!
						this.ventor = "UNKNOWN";
						return;
					}
					break;
				case "VENTOR:":
					this.ventorID=Integer.decode("0x"+tokens[i+1]);
					this.ventor = getVentor(this.ventorID);
					i+=2;
					break;
				case "PES:":
					//ignore PES
					i+=2;
					break;
				case "FUSEMAP:":
					//read fuses!
					i+=1;
					readingFuses=true;
					break;
				case "UES:":
					this.readUES(tokens[i+1]);
					i+=2;
					break;
				case "CFG:":
					this.readCFG(tokens[i+1]);
					i+=2;
					break;
				case "END": //stopp reading
					return;
				default:
					i++;
					break;
				}
			}
		}
	}
	
	public void printGAL() {
		System.out.println("GAL: "+this.name+" Ventor: "+this.ventor);
		System.out.println("Fuses:");
		for(int i=0; i< this.fuses.length;++i) {
			for(int j=0; j< this.fuses[i].length; ++j) {
				if(this.fuses[i][j])
					System.out.print("1");
				else
					System.out.print("0");
			}
			System.out.println();
		}
		System.out.println("UES:");
		for(int j=0; j< this.UES.length; ++j) {
			if(this.UES[j])
				System.out.print("1");
			else
				System.out.print("0");
		}
		System.out.println();
		System.out.println("CFG:");
		for(int j=0; j< this.config.length; ++j) {
			if(this.config[j])
				System.out.print("1");
			else
				System.out.print("0");
		}
		System.out.println();
	}
}
	
